<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Hestec\ElementalExtensions\Elements\ElementPhotoCollage;
use SilverStripe\Forms\FieldList;
use SilverStripe\Assets\Image;
use SilverStripe\Security\Permission;

class CollagePhoto extends DataObject {

    private static $table_name = 'HestecElementCollagePhoto';

    private static $singular_name = 'CollagePhoto';
    private static $plural_name = 'CollagePhotos';

    private static $db = [
        'Title' => 'Varchar(255)',
        'Sort' => 'Int'
    ];

    private static $has_one = [
        'ElementPhotoCollage' => ElementPhotoCollage::class,
        'CollagePhoto' => Image::class
    ];

    private static $owns = [
        'CollagePhoto'
    ];

    private static $summary_fields = [
        'getThumbnail' => 'Photo',
        'Title' => 'Title'
    ];

    function getThumbnail()
    {
        if ($Image = $this->CollagePhoto())
        {
            return $Image->CMSThumbnail();
        }
        else
        {
            return '(No Image)';
        }
    }

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', "Title");
        $CollagePhotoField = UploadField::create('CollagePhoto', "CollagePhoto");

        return new FieldList(
            $TitleField,
            $CollagePhotoField
        );

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
