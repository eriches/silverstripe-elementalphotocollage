<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Hestec\ElementalExtensions\Dataobjects\CollagePhoto;

class ElementPhotoCollage extends BaseElement
{

    private static $table_name = 'HestecElementPhotoCollage';

    private static $singular_name = 'PhotoCollage';

    private static $plural_name = 'PhotoCollages';

    private static $description = 'Element with multiple photos';

    private static $icon = 'font-icon-page-multiple';

    private static $has_many = array(
        'CollagePhotos' => CollagePhoto::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        if ($this->ID) {

            $CollagePhotosGridField = GridField::create(
                'CollagePhotos',
                'CollagePhotos',
                $this->CollagePhotos(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $CollagePhotosGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'PhotoCollage';
    }
}